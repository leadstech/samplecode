//
//  InfoTableViewCell.swift
//  Sample Project
//
//  Created by Ourangzaib khan on 2/9/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
  
    
    // MARK: - Class  Variables
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
