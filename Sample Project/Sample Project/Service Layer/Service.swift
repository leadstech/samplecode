//
//  Service.swift
//  Sample Project
//
//  Created by Ourangzaib khan on 2/9/16.
//  Copyright © 2016 Ourangzaib khan. All rights reserved.
//

import UIKit

class Service: NSObject {
    private let persistencyManager: PersistenManager
    private let networkManager: NetworkManager
    
    
    
    //For signleton class and sharedinstance
    class var sharedInstance: Service {
        struct Singleton {
            static let instance = Service()
        }
        return Singleton.instance
    }

    
    
    override init() {
        persistencyManager = PersistenManager()
        networkManager = NetworkManager()
        super.init()
    }
    
    
    
    func getInfo(completionHandler: (NSMutableArray?, NSError?) -> ()){
        networkManager.siteInfo(completionHandler);
    }
    
    
    
    
}
